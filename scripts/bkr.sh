#!/usr/bin/env bash

function openhab_backup () {
    #####################################################
    # Check if OpenHAB config folder is commited in Git
    #####################################################
    cd /home/openhabian/openhab-2.5.11/conf

    OH_CONF_SAVED=$(git status | grep 'working tree clean' | wc -l)
    if [ $OH_CONF_SAVED -ne 1 ]; then
        echo 'OpenHAB configuration not saved in Git. Exiting ...'
        exit
    fi

    OH_CONF_SAVED=$(git status | grep 'Your branch is ahead' | wc -l)
    if [ $OH_CONF_SAVED -eq 1 ]; then
        echo 'OpenHAB configuration not pushed to BitBucket. Exiting ...'
        exit
    fi


    #####################################################
    # Date with Timestamp
    #####################################################
    echo "+-+-+-+-+-+-+ Set Timestamp  +-+-+-+-+-+-+-+-+"
    DATE=`date +%Y_%m_%d-%H_%M_%S`


    #####################################################
    # Backup IOTstack
    #####################################################
    if [ -d /home/openhabian/IOTstack ]; then
        echo "+-+-+-+-+-+-+ IOTstack Backup +-+-+-+-+-+-+-+"
        sudo chown -R openhabian:openhabian /home/openhabian/IOTstack/volumes

        cd /home/openhabian/IOTstack/

        ./scripts/backup.sh 1 openhabian
        zip -m -r /home/openhabian/backups/tmp/iotstack-backup-$DATE.zip backups/
    fi


    #####################################################
    # Create archive file
    #####################################################
    echo "+-+-+-+-+-+-+-+ Packing backup +-+-+-+-+-+-+-+-+"
    cd /home/openhabian/backups/tmp
    find . -type f -iname "*.zip" -exec tar -rvf OpenHAB-$DATE.tar {} \;
    find . -type f -iname "*.zip" -exec rm -rf {} \;

    cd /home/openhabian/backups
    mv /home/openhabian/backups/tmp/*.tar ./
    rm -rf /home/openhabian/backups/tmp


    #####################################################
    # Save backup on Dropbox
    #####################################################
    echo "+-+-+-+-+-+-+-+ Save backup on Dropbox +-+-+-+-+-+-+-+-+"
    location=$(hostname)
    rclone sync -v /home/openhabian/backups dropbox:/$location
}

function openhab_restore () {
    #####################################################
    # Sync backups from Dropbox
    #####################################################
    echo "+-+-+-+-+-+-+-+ Restore  backups from Dropbox +-+-+-+-+-+-+-+-+"
    location=$(hostname)
    rclone sync -v dropbox:/$location /home/openhabian/backups

    backup=$(ls -ltrh /home/openhabian/backups | grep -v tmp | tail -1 | awk '{print $9}')

    cd /home/openhabian/backups/tmp


    #####################################################
    # Restore IOTstack
    #####################################################
    arch_file=$(tar -tf /home/openhabian/backups/$backup | grep iotstack)
    if [ ! -z "$arch_file"  ]; then
        echo "+-+-+-+-+-+-+ IOTstack Restore +-+-+-+-+-+-+-+-+"

        #uncpmpress archive
        tar -xvf /home/openhabian/backups/$backup $arch_file > /dev/null
        unzip $arch_file > /dev/null
        rm -rf $arch_file

        #prepare for restore
        cp -p /home/openhabian/backups/tmp/backups/backup/* /home/openhabian/backups/tmp/ 2>/dev/null
        cp -p /home/openhabian/backups/tmp/backups/rolling/* /home/openhabian/backups/tmp/ 2>/dev/null
        rm -rf /home/openhabian/backups/tmp/backups

        bk=$(ls -ltrh /home/openhabian/backups/tmp | grep -v tmp | tail -1 | awk '{print $9}')
        echo "Restoring from $bk, please be patient"

        #get a fresh copy of IOTstack
        if [ -f /home/openhabian/IOTstack/docker-compose.yml ]; then
            docker-compose --project-directory /home/openhabian/IOTstack/ down
            echo 'y' | docker system prune --volumes
            # echo 'y' | docker image prune -a
        fi

        sudo rm -rf /home/openhabian/IOTstack
        git clone https://github.com/SensorsIot/IOTstack.git /home/openhabian/IOTstack
        curl -L https://bitbucket.org/lirimia/openhab_install/raw/master/IOTstack/compose-override.yml -o /home/openhabian/IOTstack/docker-compose.override.yml

        HOSTNAME_LOWER=$(hostname | tr '[:upper:]' '[:lower:]')
        sed -i "s|conext-me|$HOSTNAME_LOWER|" /home/openhabian/IOTstack/docker-compose.override.yml

        sed -i "s|YOUR_DOMAINS|$HOSTNAME_LOWER.duckdns.org|" /home/openhabian/IOTstack/duck/duck.sh
        sed -i "s|YOUR_DUCKDNS_TOKEN|5d4a21d0-8de9-4593-8a38-74ec442b7b60|" /home/openhabian/IOTstack/duck/duck.sh

        #restore IOTstack from backup
        mkdir /home/openhabian/IOTstack/backups
        cp /home/openhabian/backups/tmp/$bk /home/openhabian/IOTstack/backups/backup.tar.gz
        rm -rf /home/openhabian/backups/tmp/*

        #restore openhab-2.5.11 config from BitBucket
        sudo rm -rf /home/openhabian/openhab-2.5.11/*
        git clone https://lirimia@bitbucket.org/lirimia/openhab_config.git --branch $(hostname) /home/openhabian/openhab-2.5.11/conf/

        #start containers
        cd /home/openhabian/IOTstack
        echo 'y' | ./scripts/restore.sh
        sudo service docker restart
        docker-compose up -d --remove-orphans

        #create openhab-2.5.11 links
        sleep 15

        docker exec -it openhab-2.5.11 mkdir /usr/share/openhab2
        docker exec -it openhab-2.5.11 ln -s /openhab/addons/ /usr/share/openhab2/

        docker exec -it openhab-2.5.11 mkdir -p /var/lib/openhab2/tmp
        docker exec -it openhab-2.5.11 ln -s /openhab/userdata/tmp/bundles/ /var/lib/openhab2/tmp/

        #update tasmoadmin container to grab update firmware from github
        /home/openhabian/scripts/tasmoadminUpdateRepository.sh

        cd /home/openhabian/backups/tmp
    fi

    #cleanup
    rm -rf /home/openhabian/backups/tmp
    
    docker-compose down
    sudo chown -R openhabian:openhabian /home/openhabian/IOTstack/volumes

    sudo reboot
}

if [ ! -d /home/openhabian/backups ]; then
    mkdir /home/openhabian/backups
fi

if test $# -eq 1; then
    case "$1" in
        --save)
            openhab_backup
            ;;
        --restore)
            openhab_restore
            ;;
        --fix)
            openhab_fix
            ;;
        --clear-cache)
            openhab_clear
            ;;
        *)
            echo 'Usage: bkr [--save | --restore | --fix | --clear-cache]'
            ;;
    esac
else
    echo 'Usage: bkr [--save | --restore | --fix | --clear-cache]'
fi
